-- phpMyAdmin SQL Dump
-- version 4.3.4
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 03 2015 г., 03:31
-- Версия сервера: 5.6.22-log
-- Версия PHP: 5.5.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_local`
--

-- --------------------------------------------------------

--
-- Структура таблицы `all_accesses`
--

CREATE TABLE IF NOT EXISTS `all_accesses` (
  `type` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `all_accesses`
--

INSERT DELAYED INTO `all_accesses` (`type`, `name`) VALUES
('adm_panel_show', 'Админка - доступ к разделам админки'),
('guest_clear', 'Мини-чат - очистка'),
('guest_delete', 'Мини-чат - удаление постов'),
('obmen_dir_delete', 'Обменник - удаление папок'),
('obmen_dir_edit', 'Обменник - управление папками'),
('obmen_dir_create', 'Обменник - создание папок'),
('obmen_file_delete', 'Обменник - удаление файлов'),
('obmen_file_edit', 'Обменник - редактирование файлов'),
('obmen_komm_del', 'Обменник - удаление комментариев'),
('foto_foto_edit', 'Фотогалерея - редактирование/удаление фото'),
('foto_alb_del', 'Фотогалерея - удаление альбомов'),
('foto_komm_del', 'Фотогалерея - удаление комментариев'),
('forum_razd_create', 'Форум - создание разделов'),
('forum_for_delete', 'Форум - удаление подфорумов'),
('forum_for_edit', 'Форум - редактирование подфорумов'),
('forum_for_create', 'Форум - создание подфорумов'),
('forum_razd_edit', 'Форум - управление разделами'),
('adm_info', 'Админка - общая информация'),
('forum_them_edit', 'Форум - редактирование тем'),
('forum_them_del', 'Форум - удаление тем'),
('forum_post_ed', 'Форум - редактирование сообщений'),
('adm_statistic', 'Админка - статистика'),
('adm_banlist', 'Админка - список забаненых'),
('adm_menu', 'Админка - главное меню'),
('adm_news', 'Админка - новости'),
('adm_rekl', 'Админка - реклама'),
('adm_set_sys', 'Админка - настройки системы'),
('adm_set_loads', 'Админка - настройки обменника'),
('adm_set_user', 'Админка - пользовательские настройки'),
('adm_set_chat', 'Админка - настройки чата'),
('adm_set_forum', 'Админка - настройки форума'),
('adm_set_foto', 'Админка - настройки фотогалереи'),
('adm_forum_sinc', 'Админка - синхронизация таблиц форума'),
('adm_themes', 'Админка - темы оформления'),
('adm_log_read', 'Админка - лог действий администрации'),
('adm_log_delete', 'Админка - удаление лога'),
('adm_mysql', 'Админка - MySQL запросы'),
('adm_ref', 'Админка - рефералы'),
('adm_show_adm', 'Админка - список администрации'),
('adm_ip_edit', 'Админка - редактирование IP операторов'),
('adm_ban_ip', 'Админка - бан по IP'),
('adm_accesses', 'Привилегии групп пользователей'),
('user_delete', 'Пользователи - удаление'),
('user_mass_delete', 'Пользователи - массовое удаление'),
('user_ban_set', 'Пользователи - бан'),
('user_ban_unset', 'Пользователи - снятие бана'),
('user_prof_edit', 'Пользователи - редактирование профиля'),
('user_collisions', 'Пользователи - совпадения ников'),
('user_show_ip', 'Пользователи - показывать IP'),
('user_show_ua', 'Пользователи - показ USER-AGENT'),
('user_show_add_info', 'Пользователи - показ доп. информации'),
('guest_show_ip', 'Гости - показ IP'),
('user_change_group', 'Пользователи - смена группы привилегий'),
('user_ban_set_h', 'Пользователи - бан max 1 сутки'),
('forum_post_close', 'Форум - возможность писать в закрытой теме'),
('user_change_nick', 'Пользователи - смена ника'),
('modules_install', 'Модули - Установка модулей'),
('modules', 'Модули - доступ к разделам модулей'),
('modules_edit', 'Модули - Управление разделами'),
('smiles', 'Смайлы - Управление смайлами'),
('cache_set', 'Кэш - Настройки'),
('cron_system', 'Планировщик задач Cron- Настройка'),
('block_status', 'Статусы - Блокировка'),
('mailGuard', 'mailGuard - Настройки'),
('system_jurnal', 'Системный журнал'),
('api_config', 'API Доступ к разделам и настройке'),
('app_adm', 'Доступ к админки приложений'),
('cmod', 'Cmod Права доступа'),
('info_system', 'Информация о системе'),
('dell_avatar', 'Удаление аватаров в анкете'),
('no_captcha', 'Отмена ввода капчи в админки'),
('edit_info_menu', 'Управление пунктами профиля'),
('license', 'Доступ к лицензии'),
('license_support', 'Служба поддержки Fiera'),
('ava_dell', 'Профиль - удаление аватаров'),
('user_cab', 'Управление пунктами кабинета'),
('all_guard', 'Защита сайта - Настройки');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `all_accesses`
--
ALTER TABLE `all_accesses`
  ADD KEY `type` (`type`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
